# Introduccción a los Sistemas Operativos

## 2019 Semestre 2

En este respositorio se irá publicando material de la materia (slides, código, tutoriales, etc).


## Listas

- tpi-est-so@listas.unq.edu.ar
        Dudas, consultas, avisos para comunicación con todos los estudiantes y docentes.

- tpi-doc-so@listas.unq.edu.ar
 Cualquier duda de la materia que no es para todos los compañeros, envíenla a esta lista

    Hasta que se habilite la lista  enviar el mail con copia a: 
    - Comision Práctica Miércoles: J Tondato , matiasmpereira@gmail.com 
    - Comision Práctica Jueves:  fernando.garcia@unq.edu.ar , estebandh@gmail.com 


## Libros usados en la cursada

- Modern Operating Systems (Tanenbaum)
- Operating System Concepts (Silberschatz)


## Slides de las Clases

- [00-curso](./teoria/00-curso.pdf)
- [01-intro](./teoria/01-intro.pdf)
- [02-processes](./teoria/02-processes.pdf)
- [03-threads](./teoria/03-threads.pdf)

## Trabajos Prácticos

Durante la materia iremos trabajando sobre un simulador básico de un sistema operativo que nos permita entender los conceptos que iremos desarrollando a lo largo de las clases. Como hemos mencionado, a esta altura carecemos de las herramientas para trabajar sobre un sistema operativo, aún de propósito didáctico como por ejemplo NachOS. Por tal motivo elegimos trabajar con un simulador que nos permita enfocarnos en los conceptos estudiados.

- [Intro a Python](./python/python_intro.md)

Breve (muy breve) introducción a Python y al trabajo práctico.

- [Ejemplos](./python/examples)

Algunos ejemplos de Python utilizados en la Introducción.



### Git

- [Git tutorial](http://rogerdudler.github.io/git-guide/)


## Slides de Prácticas
- [Curso](./practicas/slides/00_curso.pdf)
- [Git](./practicas/slides/00_git.pdf)
- [practica 1](./practicas/slides/practica1.pdf)
- [practica 2](./practicas/slides/practica2.pdf)

## Prácticas
- [Práctica 1 - Un Simulador "Extremadamente" Simplificado.](./practicas/practica_1) 
- [Práctica 2 - Procesos - Clock - Interrupcion #KILL ](./practicas/practica_2) 



## Fechas de Entrega por Comision de Práctica 

| C2 miercoles  | C1 Jueves  | Practica   | Path |
| ------------- | ---------- | --------   | ---------- |
|    04/09      | 04/09      | Práctica 1 |  [REPO_GRUPO]/practicas/practica_1 |
|    11/09      | 11/09      | Práctica 1 |  [REPO_GRUPO]/practicas/practica_2 |


